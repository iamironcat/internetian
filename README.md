# Main system - player
- 3 slots
- Each slot have health and attack/defense points
- Slots have a deck of cards
- One card per turn
- Each turn will use new cards for all slots
- Slots out of HP won't draw new cards

# Slots details
- head = effect
- body = defense
- tail = attack

# Battle
- Confirm card position or go back
- Deck cards can be damaged
- Some cards can damage slots next to damaged one

###

# Common links

Development board:
https://trello.com/b/tm3gKXOS/internetian

Devlog:
http://internetiangame.tumblr.com/